# Wordpress Simple Order #
Simple one-page order without cart.

## HowTo: ##
1. Initialize thickbox (if you don't do it somethime) in functions.php file:
```php
<?php
add_action('init', 'init_theme_method');
 
function init_theme_method() {
   add_thickbox();
}
?>
```

2. Insert **orderButton.php**, **js folder** into your theme dir.

3. Copy code from **fragments.php** to your files (in my case it was content-product.php, header.php and footer.php respectively.