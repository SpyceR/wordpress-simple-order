<!------------------------------------------------------------------

Author: SpyceR
You need to change 63th line, but feel free to edit or make any changes you deem necessary.
!!
!!IMPORTANT: It's not full file. Only code fragments. Insert them into the right places of your files
!!

------------------------------------------------------------------>

<!--
This block creates 'Add to order' button under every product/post so it must be inside the loop.
I use Woocommerce plugin as a product-manager. If you are using another one, '$product->id' & '$product->price' should be replaced by right functions.
-->
<div class="add-to-order-div">
	<input type="submit" class="button-add add-to-order" name="<?php echo $product->id ?>-<?php echo $product->price ?>" value="Add To Order" />
</div>

<!--
This block creates two buttons: one with current total of order (it's not a button at all) and main 'ORDER'-button which opens modal window with contact form.
Insert it somewhere in the header area.
!!
!!IMPORTANT: order-button uses Wordpress thickbox. Read more information about this jQuery library if you want to change something here.
!!
-->
<div>
	<?php //if (is_product_category()) { ?> 
		<div>
			<div class="total-amount-of-order">
				<span class="text-part">TOTAL: <span id="total-amount" class="num-part">0.00</span> $</span>
			</div>
		</div>
		<div>
			<div class="order-button-div">
				<input alt="#TB_inline?height=321&amp;width=400&amp;inlineId=order-form-window" title="" class="order-button thickbox" type="button" value="Order products" /></div>
			</div> 
	<?php //} ?>		
</div>

<!--
This block creates modal window with CF inside and includes js-script into page. Insert it somewhere in the footer (before </html> tag for example).
'required' attribute means that an input field must be filled out before submitting the form.
-->
<?php //if (is_product_category()) { ?> 
<div id="order-form-window" style="display:none">
	<form method="POST" id="order-form">
		<span>Your Name</span><br>
		<input type="text" name="CFname" required x-autocompletetype="name"><br>
		<span>Your phone</span><br>
		<input type="text" name="CFphone" required x-autocompletetype="tel"><br>
		<span>Your email</span><br>
		<input type="email" name="CFemail" required x-autocompletetype="email"><br>
		<input type="submit" value="SEND">
	</form>		
	<div id="successAnswer">
	<p>Thank you for order! We will contact you soon!</p>
	</div>
	<div id="errorAnswer">
	<p>Sorry, an error occurred. Try later.</p>
	</div>
</div>
	
<script type="text/javascript" src="PATH-TO-JS-FILE/js/addToOrder.js"></script>
<?php //} ?>


